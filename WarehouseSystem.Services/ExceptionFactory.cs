﻿using System;
using WarehouseSystem.Models;

namespace WarehouseSystem.Services
{
    public class ExceptionFactory
    {
        public static QueryException CreatePackageTypesNotFoundException()
        {
            return new QueryException("Package Types not found", ResponseStatusCode.NotFound);
        }

        public static QueryException CreatePackageTypeByIdNotFoundException(Guid id)
        {
            return new QueryException($"Package Type with {id} not found", ResponseStatusCode.NotFound);
        }

        public static QueryException CreatePackageTypeNotCreatedException()
        {
            return new QueryException("Package Type not Created", ResponseStatusCode.Conflict);
        }

        public static QueryException CreatePackageNotFoundException()
        {
            return new QueryException("Package not found", ResponseStatusCode.NotFound);
        }

        public static QueryException CreatePackageByIdNotFoundException(Guid id)
        {
            return new QueryException($"Package with {id} not found", ResponseStatusCode.NotFound);
        }

        public static QueryException CreatePackageNotCreatedException()
        {
            return new QueryException("Package not Created", ResponseStatusCode.Conflict);
        }
    }
}
