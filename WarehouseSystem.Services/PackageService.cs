﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;

namespace WarehouseSystem.Services
{
    public class PackageService : IPackageService
    {
        private readonly IPackageRepository _packageRepository;

        public PackageService(IPackageRepository packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public List<Package> GetPackages(string searchTerm)
        {
            var packages = _packageRepository.GetPackages(searchTerm);

            if (packages.Count == 0)
            {
                throw ExceptionFactory.CreatePackageNotFoundException();
            }

            return packages;
        }

        public Package GetPackageById(Guid id)
        {
            var package = _packageRepository.GetPackageById(id);

            if (package == null)
            {
                throw ExceptionFactory.CreatePackageByIdNotFoundException(id);
            }

            return package;
        }

        public bool DeletePackage(Guid id)
        {
            var result = GetPackageById(id);

            if (result == null)
            {
                throw ExceptionFactory.CreatePackageByIdNotFoundException(id);
            }

            return _packageRepository.DeletePackage(id);
        }

        public bool UpdatePackage(Package package)
        {
            var result = GetPackageById(package.Id);

            if (result == null)
            {
                throw ExceptionFactory.CreatePackageByIdNotFoundException(package.Id);
            }

            return _packageRepository.UpdatePackage(package);
        }

        public Guid CreatePackage(Package package)
        {
            var result = _packageRepository.CreatePackage(package);
            var packageById = _packageRepository.GetPackageById(result);

            if (packageById == null)
            {
                throw ExceptionFactory.CreatePackageNotCreatedException();
            }

            return result;
        }
    }
}
