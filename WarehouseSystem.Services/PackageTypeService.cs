﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;

namespace WarehouseSystem.Services
{
    public class PackageTypeService : IPackageTypeService
    {
        private readonly IPackageTypeRepository _packageTypeRepository;

        public PackageTypeService(IPackageTypeRepository packageTypeRepository)
        {
            _packageTypeRepository = packageTypeRepository;
        }

        public List<PackageType> GetPackageTypes(string searchTerm)
        {
            var packageTypes = _packageTypeRepository.GetPackageTypes(searchTerm);

            if (packageTypes.Count == 0)
            {
                throw ExceptionFactory.CreatePackageTypesNotFoundException();
            }

            return packageTypes;
        }

        public PackageType GetPackageTypeById(Guid id)
        {
            var package = _packageTypeRepository.GetPackageTypeById(id);

            if (package == null)
            {
                throw ExceptionFactory.CreatePackageTypeByIdNotFoundException(id);
            }

            return package;
        }

        public Guid CreatePackageType(PackageType packageType)
        {
            var result = _packageTypeRepository.CreatePackageType(packageType);
            var packageTypeById = _packageTypeRepository.GetPackageTypeById(result);

            if (packageTypeById == null)
            {
                throw ExceptionFactory.CreatePackageTypeNotCreatedException();
            }

            return result;
        }

        public bool DeletePackageType(Guid id)
        {
            var result = GetPackageTypeById(id);

            if (result == null)
            {
                throw ExceptionFactory.CreatePackageTypeByIdNotFoundException(id);
            }

            return _packageTypeRepository.DeletePackageType(id);
        }

        public bool UpdatePackageType(PackageType packageType)
        {
            var result = GetPackageTypeById(packageType.Id);

            if (result == null)
            {
                throw ExceptionFactory.CreatePackageTypeByIdNotFoundException(packageType.Id);
            }

            

            return _packageTypeRepository.UpdatePackageType(packageType);
        }
    }
}
