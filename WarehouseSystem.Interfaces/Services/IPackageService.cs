﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Models;

namespace WarehouseSystem.Interfaces.Services
{
    public interface IPackageService
    {
        List<Package> GetPackages(string searchTerm);
        Package GetPackageById(Guid id);
        Guid CreatePackage(Package package);
        bool DeletePackage(Guid id);
        bool UpdatePackage(Package package);
    }
}
