﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Models;

namespace WarehouseSystem.Interfaces.Services
{
    public interface IPackageTypeService
    {
        List<PackageType> GetPackageTypes(string searchTerm);
        PackageType GetPackageTypeById(Guid id);
        Guid CreatePackageType(PackageType packageType);
        bool DeletePackageType(Guid id);
        bool UpdatePackageType(PackageType packageType);
    }
}
