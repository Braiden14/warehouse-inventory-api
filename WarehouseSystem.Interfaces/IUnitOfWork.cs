﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;

namespace WarehouseSystem.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void CommitChanges();
        void RollbackChanges();
        int Execute(string query, object param, CommandType commandType = CommandType.Text);
        IEnumerable<T> Query<T>(string query, object param, CommandType commandType = CommandType.Text);
        IEnumerable<TAb> Query<TA, TB, TAb>(string query, Func<TA, TB, TAb> map, object param = null, CommandType commandType = CommandType.Text, string splitOn = "Id");
        IEnumerable<TAbc> Query<TA, TB, TC, TAbc>(string query, Func<TA, TB, TC, TAbc> map, object param = null, CommandType commandType = CommandType.Text, string splitOn = "Id");
        IEnumerable<dynamic> Query(string query, object param, CommandType commandType = CommandType.Text);
        Dictionary<TKey, TValue> Query<TKey, TValue>(string query, object param, CommandType commandType = CommandType.Text);
        SqlMapper.GridReader QueryMultiple(string query, object param, CommandType commandType = CommandType.Text, int commandTimeout = 30);
    }
}
