﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Models;

namespace WarehouseSystem.Interfaces.Repositories
{
    public interface IPackageRepository
    {
        List<Package> GetPackages(string searchTerm);
        Package GetPackageById(Guid id);
        Guid CreatePackage(Package package);
        bool DeletePackage(Guid id);
        bool UpdatePackage(Package package);
    }
}
