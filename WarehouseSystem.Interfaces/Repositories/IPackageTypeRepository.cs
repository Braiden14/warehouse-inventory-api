﻿using System;
using System.Collections.Generic;
using WarehouseSystem.Models;

namespace WarehouseSystem.Interfaces.Repositories
{
    public interface IPackageTypeRepository
    {
        List<PackageType> GetPackageTypes(string searchTerm);
        PackageType GetPackageTypeById(Guid id);
        Guid CreatePackageType(PackageType package);
        bool DeletePackageType(Guid id);
        bool UpdatePackageType(PackageType packageType);
    }
}