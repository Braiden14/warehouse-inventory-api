﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Repositories;
using WarehouseSystem.Services;

namespace WarehouseSystem.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddMvc();
            services.Configure<MvcOptions>(options =>
                {
                    options.Filters.Add(new CorsAuthorizationFilterFactory("MyPolicy"));
                });
            services.Configure<IISOptions>(options => { options.ForwardClientCertificate = false; });
            services.AddSwaggerGen(x => { x.SwaggerDoc("v1", new Info { Title = "Warehouse Api" }); });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            // Enable Cors
            app.UseCors("MyPolicy");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(x => { x.SwaggerEndpoint("/swagger/v1/swagger.json", "Warehouse Api"); });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var connectionString = Configuration.GetSection("ConnectionStrings:DefaultConnection").Value;

            builder.RegisterType<PackageRepository>()
                .As<IPackageRepository>()
                .InstancePerLifetimeScope()
                .WithParameter("warehouseDatabaseConnectionString", connectionString);

            builder.RegisterType<PackageService>()
                .As<IPackageService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<PackageTypeRepository>()
                .As<IPackageTypeRepository>()
                .InstancePerLifetimeScope()
                .WithParameter("warehouseDatabaseConnectionString", connectionString);

            builder.RegisterType<PackageTypeService>()
                .As<IPackageTypeService>()
                .InstancePerLifetimeScope();
        }
    }
}
