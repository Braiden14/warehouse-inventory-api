﻿using System;
using Microsoft.AspNetCore.Mvc;
using WarehouseSystem.Api.Filters;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;

namespace WarehouseSystem.Api.Controllers
{
    [WebApiExceptionFilter]
    [Route("api/Packages")]
    public class PackageController : Controller
    {
        private readonly IPackageService _packageService;

        public PackageController(IPackageService packageService)
        {
            _packageService = packageService;
        }

        [HttpGet]
        public ActionResult GetPackages(string searchTerm)
        {
            return Ok(_packageService.GetPackages(searchTerm));
        }

        [HttpGet("{id}")]
        public ActionResult GetPackageById(Guid id)
        {
            return Ok(_packageService.GetPackageById(id));
        }

        [HttpGet("Ping")]
        public ActionResult GetPackagePing()
        {
            return Ok("Pong");
        }

        [HttpPost]
        public ActionResult CreatePakage([FromBody] Package package)
        {
            return Ok(_packageService.CreatePackage(package));
        }

        [HttpPut("{id:Guid}")]
        public ActionResult UpdatePackage(Guid id, [FromBody] Package package)
        {
            package.Id = id;
            _packageService.UpdatePackage(package);

            return Ok("Package Updated");
        }

        [HttpDelete("{id:Guid}")]
        public ActionResult DeletePackage(Guid id)
        {
            _packageService.DeletePackage(id);

            return Ok("Package deleted");
        }
    }
}