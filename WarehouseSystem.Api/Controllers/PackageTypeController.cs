﻿using System;
using Microsoft.AspNetCore.Mvc;
using WarehouseSystem.Api.Filters;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;

namespace WarehouseSystem.Api.Controllers
{
    [WebApiExceptionFilter]
    [Route("api/PackageType")]
    public class PackageTypeController : Controller
    {
        private readonly IPackageTypeService _packageTypeService;

        public PackageTypeController(IPackageTypeService packageTypeService)
        {
            _packageTypeService = packageTypeService;
        }

        [HttpGet]
        public ActionResult GetPackageTypes(string searchTerm)
        {
            return Ok(_packageTypeService.GetPackageTypes(searchTerm));
        }

        [HttpGet("{id}")]
        public ActionResult GetPackageTypeById(Guid id)
        {
            return Ok(_packageTypeService.GetPackageTypeById(id));
        }

        [HttpPost]
        public ActionResult CreatePakage([FromBody] PackageType packageType)
        {
            return Ok(_packageTypeService.CreatePackageType(packageType));
        }

        [HttpPut("{id:Guid}")]
        public ActionResult UpdatePackage(Guid id, [FromBody] PackageType packageType)
        {
            packageType.Id = id;
            _packageTypeService.UpdatePackageType(packageType);

            return Ok("Package Type Updated");
        }

        [HttpDelete("{id:Guid}")]
        public ActionResult DeletePackage(Guid id)
        {
            _packageTypeService.DeletePackageType(id);

            return Ok("Package Type deleted");
        }
    }
}