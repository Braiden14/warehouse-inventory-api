﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using WarehouseSystem.Models;

namespace WarehouseSystem.Api.Filters
{
    public class WebApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            LoadDependencies(actionExecutedContext);

            if (actionExecutedContext.Exception.GetType() == typeof(QueryException))
            {
                var queryException = (QueryException)actionExecutedContext.Exception;
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                        MapResponseStatusCodeToHttpStatusCode(queryException.StatusCode), queryException.Message);
                return;
            }

            actionExecutedContext.Response =
                actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Internal Server Error");
        }

        private void LoadDependencies(HttpActionExecutedContext actionExecutedContext)
        {
        }

        private HttpStatusCode MapResponseStatusCodeToHttpStatusCode(ResponseStatusCode queryExceptionStatusCode)
        {
            switch (queryExceptionStatusCode)
            {
                case ResponseStatusCode.Unused:
                    return HttpStatusCode.Unused;
                case ResponseStatusCode.NotFound:
                    return HttpStatusCode.NotFound;
                case ResponseStatusCode.Conflict:
                    return HttpStatusCode.Conflict;
                case ResponseStatusCode.BadRequest:
                    return HttpStatusCode.BadRequest;
                case ResponseStatusCode.Forbidden:
                    return HttpStatusCode.Forbidden;
                case ResponseStatusCode.Unauthorized:
                    return HttpStatusCode.Unauthorized;
            }

            return HttpStatusCode.Unused;
        }
    }
}
