﻿using System;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;
using WarehouseSystem.Services;

namespace WarehouseSystem.Tests.Services
{
    public class PackageTypeServiceTests
    {
        private IPackageTypeService _packageTypeService;
        private readonly IPackageTypeRepository _packageTypeRepository = Substitute.For<IPackageTypeRepository>();

        public List<PackageType> PackageTypes = new List<PackageType>
        {
            new PackageType {Id = new Guid()},
            new PackageType {Id = new Guid()},
            new PackageType {Id = new Guid()},
            new PackageType {Id = new Guid()}
        };

        [SetUp]
        public void Initialise()
        {
            _packageTypeService = new PackageTypeService(_packageTypeRepository);
        }

        [Test]
        public void Given_String_That_DoesNotExistInDatabase_When_GettingPackages_Then_ThrowException()
        {
            _packageTypeRepository.GetPackageTypes(Arg.Any<string>()).Returns(new List<PackageType>());

            var searchTerm = Guid.NewGuid().ToString();
            var result = Assert.Throws<QueryException>(() => _packageTypeService.GetPackageTypes(searchTerm));

            Assert.AreEqual("Package Types not found", result.Message);
        }

        [Test]
        public void Given_Id_That_Does_Not_Exist_When_GettingPackageById_Then_ThrowException()
        {
            var guid = Guid.NewGuid();
            var result = Assert.Throws<QueryException>(() => _packageTypeService.GetPackageTypeById(guid));

            Assert.AreEqual($"Package Type with {guid} not found", result.Message);
        }

        [Test]
        public void Given_Id_That_Does_Not_Exist_When_DeletingAPackage_Then_ThrowException()
        {
            var guid = Guid.NewGuid();
            var result = Assert.Throws<QueryException>(() => _packageTypeService.DeletePackageType(guid));

            Assert.AreEqual($"Package Type with {guid} not found", result.Message);
        }

        [Test]
        public void Given_Empty_String_When_GettingPackages_Then_Return_Package_List()
        {
            _packageTypeRepository.GetPackageTypes(Arg.Any<string>()).Returns(PackageTypes);

            var packageTypes = _packageTypeService.GetPackageTypes("");

            Assert.IsTrue(packageTypes.Count > 0);
        }
    }
}