﻿using System;
using System.Collections.Generic;
using Castle.Components.DictionaryAdapter;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Models;
using WarehouseSystem.Services;

namespace WarehouseSystem.Tests.Services
{
    [TestFixture]
    public class PackageServiceTests
    {
        private IPackageService _packageService;
        private readonly IPackageRepository _packageRepository = Substitute.For<IPackageRepository>();

        public List<Package> Packages = new List<Package>
        {
            new Package {Id = new Guid()},
            new Package {Id = new Guid()},
            new Package {Id = new Guid()},
            new Package {Id = new Guid()}
        };

        [SetUp]
        public void Initialise()
        {
            _packageService = new PackageService(_packageRepository);
        }

        [Test]
        public void Given_String_That_DoesNotExistInDatabase_When_GettingPackages_Then_ThrowException()
        {
            _packageRepository.GetPackages(Arg.Any<string>()).Returns(new EditableList<Package>());

            var searchTerm = Guid.NewGuid().ToString();
            var result = Assert.Throws<QueryException>(() => _packageService.GetPackages(searchTerm));

            Assert.AreEqual("Package not found", result.Message);
        }

        [Test]
        public void Given_Id_That_Does_Not_Exist_When_GettingPackageById_Then_ThrowException()
        {
            _packageRepository.GetPackageById(Arg.Any<Guid>()).ReturnsNull();

            var guid = Guid.NewGuid();
            var result = Assert.Throws<QueryException>(() => _packageService.GetPackageById(guid));

            Assert.AreEqual($"Package with {guid} not found", result.Message);
        }

        [Test]
        public void Given_Id_That_Does_Not_Exist_When_DeletingAPackage_Then_ThrowException()
        {
            _packageRepository.DeletePackage(Arg.Any<Guid>()).Returns(false);

            var guid = Guid.NewGuid();
            var result = Assert.Throws<QueryException>(() => _packageService.DeletePackage(guid));

            Assert.AreEqual($"Package with {guid} not found", result.Message);
        }

        [Test]
        public void Given_Empty_String_When_GettingPackages_Then_Return_Package_List()
        {
            _packageRepository.GetPackages(Arg.Any<string>()).Returns(Packages);

            var packages = _packageService.GetPackages("");

            Assert.IsTrue(packages.Count > 0);
        }
    }
}