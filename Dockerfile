FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/WarehouseSystem.Api/out .
ENTRYPOINT ["dotnet", "WarehouseSystem.Api.dll"]
