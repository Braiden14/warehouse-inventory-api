﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Repositories;

namespace WarehouseSystem.IntegrationTests.Package
{
    [TestFixture]
    public class PackageApiTests
    {
        private RestClient _restClient;
        private IPackageRepository _packageRepository;
        private const string ConnectionString = "Data Source=BARCELONA;Initial Catalog=BraidensWarehouse;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        [SetUp]
        public void InitialiseTest()
        {
            _restClient = new RestClient("http://localhost:53019/api");
            _packageRepository = new PackageRepository(ConnectionString);
        }

        [Test]
        public void Given_SearchTerm_Then_CallsApi_And_ReturnsFilteredPackages()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true,
                Volume = 0
            };

            package.Id = _packageRepository.CreatePackage(package);

            Assert.IsTrue(GetPackages("Letter") != null);

            _packageRepository.DeletePackage(package.Id);
        }

        [Test]
        public void Given_PackageId_Then_CallsApi_And_Returns_Package()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true,
                Volume = 0
            };

            package.Id = _packageRepository.CreatePackage(package);

            Assert.IsTrue(_packageRepository.GetPackageById(package.Id) != null);

            var packageFromApiCall = GetPackageById(package.Id);
            packageFromApiCall.Volume = 0; 

            Assert.AreEqual(packageFromApiCall, package);

            _packageRepository.DeletePackage(package.Id);
        }

        [Test]
        public void Given_Package_When_CallingApi_Then_AddPackage_And_Return_New_Guid()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true,
                Volume = 0
            };

            package.Id = CreatePackage(package);
            
            Assert.IsTrue(package.Id != Guid.Empty);

            var packageFromDb = _packageRepository.GetPackageById((package.Id));
            packageFromDb.Volume = package.Volume;


            Assert.AreEqual(package, packageFromDb);

            _packageRepository.DeletePackage(package.Id);
        }

        [Test]
        public void Given_Package_Details_And_PackageId_Then_Calls_Api_And_Updates_Package()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true,
                Volume = 0
            };

            package.Id = _packageRepository.CreatePackage(package);

            Assert.IsTrue(package.Id != Guid.Empty);

            package.PackageTypeId = new Guid();
            package.Weight = 15;

            var result = UpdatePackage(package);

            Assert.IsTrue(result == HttpStatusCode.OK);

            var packageFromDb = _packageRepository.GetPackageById(package.Id);
            packageFromDb.Volume = 0;

            Assert.AreEqual(packageFromDb, package);

            _packageRepository.DeletePackage(package.Id);
        }

        [Test]
        public void Given_PackageId_When_CallingApi_To_DeletePackage_Then_Return_status()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true,
                Volume = 0
            };

            package.Id = _packageRepository.CreatePackage(package);

            Assert.IsTrue(_packageRepository.GetPackageById(package.Id) != null);
            Assert.IsTrue(DeletePackage(package.Id) == HttpStatusCode.OK);
            Assert.IsTrue(_packageRepository.GetPackageById(package.Id) == null);
        }

        public List<Models.Package> GetPackages(string searchTerm)
        {
            var request = new RestRequest("packages");
            request.AddQueryParameter("searchTerm", searchTerm);
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<List<Models.Package>>(response.Content);
        }

        public Guid CreatePackage(Models.Package package)
        {
            var request = new RestRequest("Packages", Method.POST) {RequestFormat = DataFormat.Json};
            request.AddBody(package);
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<Guid>(response.Content);
        }

        public Models.Package GetPackageById(Guid id)
        {
            var request = new RestRequest($"Packages/{id.ToString()}");
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<Models.Package>(response.Content);
        }

        public HttpStatusCode UpdatePackage(Models.Package package)
        {
            var request = new RestRequest($"Packages/{package.Id.ToString()}", Method.PUT) { RequestFormat = DataFormat.Json };
            request.AddBody(package);
            var response = _restClient.Execute(request);

            return response.StatusCode;
        }

        public HttpStatusCode DeletePackage(Guid id)
        {
            var request = new RestRequest($"Packages/{id.ToString()}", Method.DELETE);
            var response = _restClient.Execute(request);

            return response.StatusCode;
        }
    }
}
