﻿using System;
using NUnit.Framework;
using WarehouseSystem.Interfaces.Services;
using WarehouseSystem.Repositories;
using WarehouseSystem.Services;

namespace WarehouseSystem.IntegrationTests.Package
{
    [TestFixture]
    public class PackageServiceTests
    {
        private IPackageService _packageService;
        private const string ConnectionString = "Data Source=BARCELONA;Initial Catalog=BraidensWarehouse;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        [SetUp]
        public void Initialise()
        {
            _packageService = new PackageService(new PackageRepository(ConnectionString));
        }

        [Test]
        public void Given_Package_Details_Insert_Package_Into_Database_Confirm_Package_Exists()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 0,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true
            };

            package.Id = _packageService.CreatePackage(package);

            Assert.IsNotNull(package.Id);

            var packageFromDb = _packageService.GetPackageById(package.Id);
            packageFromDb.Volume = package.Volume;

            Assert.AreEqual(package, packageFromDb);

            _packageService.DeletePackage(package.Id);
        }

        [Test]
        public void Given_Search_String_When_Getting_Packages_Confirm_Package_Returned()
        {
            var searchTerm = "Box";

            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 0,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Stuff",
                IsFragile = true
            };

            var packageId = _packageService.CreatePackage(package);
            var results = _packageService.GetPackages(searchTerm);

            Assert.IsTrue(results.Count > 0);

            _packageService.DeletePackage(packageId);
        }

        [Test]
        public void Given_Package_Insert_Package_Then_Delete_Confirm_Package_Deleted()
        {
            var package = new Models.Package
            {
                PackageTypeId = new Guid(),
                Weight = 0,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true
            };

            var packageId = _packageService.CreatePackage(package);
            var packageFromDb = _packageService.GetPackageById(packageId);

            Assert.IsTrue(packageFromDb != null);
            Assert.IsTrue(_packageService.DeletePackage(packageId));
        }

        [Test]
        public void Given_Package_Update_Package_Then_Return_Updated_Package()
        {
            var package = new Models.Package
            {
                 PackageTypeId = new Guid(),
                Weight = 0,
                Length = 5,
                Height = 5,
                Width = 5,
                Quantity = 5,
                Contents = "Letter",
                IsFragile = true
            };

            var packageId = _packageService.CreatePackage(package);
            var packageFromDb = _packageService.GetPackageById(packageId);

            Assert.IsTrue(packageFromDb != null);

            package = new Models.Package
            {
                Id = packageId,
                PackageTypeId = new Guid(),
                Weight = 10,
                Length = 15,
                Height = 15,
                Width = 51,
                Quantity = 15,
                Contents = "Box",
                IsFragile = false
            };

            var result = _packageService.UpdatePackage(package);

            Assert.IsTrue(result);

            packageFromDb = _packageService.GetPackageById(packageId);
            packageFromDb.Volume = package.Volume;

            Assert.AreEqual(package, packageFromDb);

            _packageService.DeletePackage(packageId);
        }
    }
}
