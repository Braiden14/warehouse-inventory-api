﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Repositories;

namespace WarehouseSystem.IntegrationTests.PackageType
{
    [TestFixture]
    public class PackageTypeApiTests
    {
        private RestClient _restClient;
        private IPackageTypeRepository _packageTypeRepository;
        private readonly string _connectionString = "Data Source=BARCELONA;Initial Catalog=BraidensWarehouse;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        [SetUp]
        public void InitialiseTest()
        {
            _restClient = new RestClient("http://localhost:53019/api");
            _packageTypeRepository = new PackageTypeRepository(_connectionString);
        }

        [Test]
        public void Given_SearchTerm_Then_CallsApi_And_ReturnsFilteredPackageType()
        {
            var packageType = new Models.PackageType
            {
                Id = new Guid(),
                Type = "Crate"
            };

            packageType.Id = _packageTypeRepository.CreatePackageType(packageType);

            Assert.IsTrue(GetPackageType("Crate") != null);

            _packageTypeRepository.DeletePackageType(packageType.Id);
        }

        [Test]
        public void Given_PackageTypeId_Then_CallsApi_And_Returns_PackageType()
        {
            var packageType = new Models.PackageType
            {
                Id = new Guid(),
                Type = "Crate"
            };

            packageType.Id = _packageTypeRepository.CreatePackageType(packageType);

            Assert.IsTrue(_packageTypeRepository.GetPackageTypeById(packageType.Id) != null);

            var packageFromApiCall = GetPackageTypeById(packageType.Id);

            Assert.AreEqual(packageFromApiCall, packageType);

            _packageTypeRepository.DeletePackageType(packageType.Id);
        }

        [Test]
        public void Given_PackageType_When_CallingApi_Then_AddPackageType_And_Return_New_Guid()
        {
            var packageType = new Models.PackageType
            {
                Id = new Guid(),
                Type = "Crate"
            };

            packageType.Id = CreatePackageType(packageType);

            Assert.IsTrue(packageType.Id != Guid.Empty);

            var packageFromDb = _packageTypeRepository.GetPackageTypeById((packageType.Id));

            Assert.AreEqual(packageType, packageFromDb);

            _packageTypeRepository.DeletePackageType(packageType.Id);
        }

        [Test]
        public void Given_PackageType_Details_And_PackageTypeId_Then_Calls_Api_And_Updates_PackageType()
        {
            var packageType = new Models.PackageType
            {
                Id = new Guid(),
                Type = "Crate"
            };

            packageType.Id = _packageTypeRepository.CreatePackageType(packageType);

            Assert.IsTrue(packageType.Id != Guid.Empty);

            packageType.Type = "Letter";

            var result = UpdatePackageType(packageType);

            Assert.IsTrue(result == HttpStatusCode.OK);

            var packageFromDb = _packageTypeRepository.GetPackageTypeById(packageType.Id);

            Assert.AreEqual(packageFromDb, packageType);

            _packageTypeRepository.DeletePackageType(packageType.Id);
        }

        [Test]
        public void Given_PackageId_When_CallingApi_To_DeletePackage_Then_Return_status()
        {
            var packageType = new Models.PackageType
            {
                Id = new Guid(),
                Type = "Crate"
            };

            packageType.Id = _packageTypeRepository.CreatePackageType(packageType);

            Assert.IsTrue(_packageTypeRepository.GetPackageTypeById(packageType.Id) != null);
            Assert.IsTrue(DeletePackageType(packageType.Id) == HttpStatusCode.OK);
            Assert.IsTrue(_packageTypeRepository.GetPackageTypeById(packageType.Id) == null);
        }

        public List<Models.PackageType> GetPackageType(string searchTerm)
        {
            var request = new RestRequest("PackageType", Method.GET);
            request.AddQueryParameter("searchTerm", searchTerm);
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<List<Models.PackageType>>(response.Content);
        }

        public Models.PackageType GetPackageTypeById(Guid id)
        {
            var request = new RestRequest($"PackageType/{id.ToString()}", Method.GET);
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<Models.PackageType>(response.Content);
        }

        public Guid CreatePackageType(Models.PackageType packageType)
        {
            var request = new RestRequest("PackageType", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddBody(packageType);
            var response = _restClient.Execute(request);

            return JsonConvert.DeserializeObject<Guid>(response.Content);
        }

        public HttpStatusCode UpdatePackageType(Models.PackageType packageType)
        {
            var request = new RestRequest($"PackageType/{packageType.Id.ToString()}", Method.PUT) { RequestFormat = DataFormat.Json };
            request.AddBody(packageType);
            var response = _restClient.Execute(request);

            return response.StatusCode;
        }

        public HttpStatusCode DeletePackageType(Guid id)
        {
            var request = new RestRequest($"PackageType/{id.ToString()}", Method.DELETE);
            var response = _restClient.Execute(request);

            return response.StatusCode;
        }
    }
}
