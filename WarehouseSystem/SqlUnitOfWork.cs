﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using WarehouseSystem.Interfaces;

namespace WarehouseSystem
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        public SqlUnitOfWork(string connectionString, bool requireTransaction = true)
        {
            _connection = GetConnection(connectionString);
            _connection.Open();

            if (requireTransaction)
            {
                BeginTransaction();
            }
        }

        private IDbConnection GetConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        private void BeginTransaction()
        {
            if (_transaction != null)
            {
                return;
            }
            _transaction = _connection.BeginTransaction();
        }

        public void CommitChanges()
        {
            _transaction?.Commit();
        }

        public void RollbackChanges()
        {
            _transaction?.Rollback();
        }

        public int Execute(string query, object param = null, CommandType commandType = CommandType.Text)
        {
            return _connection.Execute(query, param, _transaction, commandType: commandType);
        }

        public IEnumerable<T> Query<T>(string query, object param = null, CommandType commandType = CommandType.Text)
        {
            return _connection.Query<T>(query, param, _transaction, commandType: commandType);
        }

        public IEnumerable<TAb> Query<TA, TB, TAb>(string query, Func<TA, TB, TAb> map, object param = null, CommandType commandType = CommandType.Text, string splitOn = "Id")
        {
            return _connection.Query(query, map, param, _transaction, commandType: commandType, splitOn: splitOn);
        }

        public IEnumerable<TAbc> Query<TA, TB, TC, TAbc>(string query, Func<TA, TB, TC, TAbc> map, object param = null, CommandType commandType = CommandType.Text, string splitOn = "Id")
        {
            return _connection.Query(query, map, param, _transaction, commandType: commandType, splitOn: splitOn);
        }

        public IEnumerable<dynamic> Query(string query, object param = null, CommandType commandType = CommandType.Text)
        {
            return _connection.Query(query, param, _transaction, commandType: commandType);
        }

        public Dictionary<TKey, TValue> Query<TKey, TValue>(string query, object param = null, CommandType commandType = CommandType.Text)
        {
            return _connection.Query(query, param, _transaction, commandType: commandType).ToDictionary(
                    row => (TKey)row.Key,
                    row => (TValue)row.Value
                );
        }

        public SqlMapper.GridReader QueryMultiple(string query, object param = null, CommandType commandType = CommandType.Text, int commandTimeout = 30)
        {
            return _connection.QueryMultiple(query, param, _transaction, commandType: commandType, commandTimeout: commandTimeout);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_transaction != null)
                {
                    _transaction.Dispose();
                    _transaction = null;
                }
                if (_connection != null)
                {
                    _connection.Dispose();
                    _connection = null;
                }
            }
        }

        ~SqlUnitOfWork()
        {
            Dispose(false);
        }
    }
}
