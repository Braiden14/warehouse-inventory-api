﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WarehouseSystem.Interfaces;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Models;

namespace WarehouseSystem.Repositories
{
    public class PackageRepository : IPackageRepository
    {
        private readonly string _warehouseDatabaseConnectionString;
        public PackageRepository(string warehouseDatabaseConnectionString)
        {
            _warehouseDatabaseConnectionString = warehouseDatabaseConnectionString;
        }

        public List<Package> GetPackages(string searchTerm)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Filter = searchTerm
                };

                return connection.Query<Package>("spGetPackages", parameters, CommandType.StoredProcedure).ToList();
            }
        }

        public Package GetPackageById(Guid id)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Id = id
                };

                return connection.Query<Package>("spGetPackageById", parameters, CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public bool DeletePackage(Guid id)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Id = id
                };

                var result = connection.Execute("spDeletePackage", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0;
            }
        }

        public bool UpdatePackage(Package package)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    package.Id,
                    package.PackageTypeId,
                    package.Weight,
                    package.Length,
                    package.Height,
                    package.Width,
                    package.Quantity,
                    package.Contents,
                    package.IsFragile
                };

                var result = connection.Execute("spUpdatePackage", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0;
            }
        }

        public Guid CreatePackage(Package package)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                package.Id = Guid.NewGuid();

                var parameters = new
                {
                    package.Id,
                    package.PackageTypeId,
                    package.Weight,
                    package.Length,
                    package.Height,
                    package.Width,
                    package.Quantity,
                    package.Contents,
                    package.IsFragile
                };

                var result = connection.Execute("spInsertPackage", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0 ? package.Id : Guid.Empty;
            }
        }
    }
}