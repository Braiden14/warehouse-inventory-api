﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WarehouseSystem.Interfaces;
using WarehouseSystem.Interfaces.Repositories;
using WarehouseSystem.Models;

namespace WarehouseSystem.Repositories
{
    public class PackageTypeRepository : IPackageTypeRepository
    {
        private readonly string _warehouseDatabaseConnectionString;

        public PackageTypeRepository(string warehouseDatabaseConnectionString)
        {
            _warehouseDatabaseConnectionString = warehouseDatabaseConnectionString;
        }

        public List<PackageType> GetPackageTypes(string searchTerm)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Filter = searchTerm
                };

                return connection.Query<PackageType>("spGetPackageTypes", parameters, CommandType.StoredProcedure).ToList();
            }
        }

        public PackageType GetPackageTypeById(Guid id)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Id = id
                };

                return connection.Query<PackageType>("spGetPackageTypeById", parameters, CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public Guid CreatePackageType(PackageType packageType)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                packageType.Id = Guid.NewGuid();

                var parameters = new
                {
                    packageType.Id,
                    packageType.Type
                };

                var result = connection.Execute("spInsertPackageType", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0 ? packageType.Id : Guid.Empty;
            }
        }

        public bool DeletePackageType(Guid id)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    Id = id
                };

                var result = connection.Execute("spDeletePackageType", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0;
            }
        }

        public bool UpdatePackageType(PackageType packageType)
        {
            using (IUnitOfWork connection = new SqlUnitOfWork(_warehouseDatabaseConnectionString))
            {
                var parameters = new
                {
                    packageType.Id,
                    packageType.Type
                };

                var result = connection.Execute("spUpdatePackageType", parameters, CommandType.StoredProcedure);
                connection.CommitChanges();

                return result > 0;
            }
        }
    }
}
