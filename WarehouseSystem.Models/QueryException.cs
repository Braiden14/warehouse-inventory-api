﻿namespace WarehouseSystem.Models
{
    public class QueryException : System.Exception
    {
        public ResponseStatusCode StatusCode { get; private set; }

        public QueryException(string message, ResponseStatusCode code = ResponseStatusCode.Unused) : base(message)
        {
            StatusCode = code;
        }
    }
}
