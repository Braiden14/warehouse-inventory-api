﻿namespace WarehouseSystem.Models
{
    public enum ResponseStatusCode
    {
        Unused,
        NotFound,
        Conflict,
        BadRequest,
        Forbidden,
        Unauthorized
    }
}
