﻿using System;

namespace WarehouseSystem.Models
{
    public class Package : IEquatable<Package>
    {
        public Guid Id { get; set; }
        public float Weight { get; set; }
        public float Length { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public int Quantity { get; set; }
        public string Contents { get; set; }
        public bool IsFragile { get; set; }
        public double Volume { get; set; }
        public Guid PackageTypeId { get; set; }

        public bool Equals(Package other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && 
                   string.Equals(PackageTypeId, other.PackageTypeId) && 
                   Weight.Equals(other.Weight) && Length.Equals(other.Length) && 
                   Height.Equals(other.Height) && Width.Equals(other.Width) && 
                   Quantity == other.Quantity && 
                   string.Equals(Contents, other.Contents) && 
                   IsFragile == other.IsFragile;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ (PackageTypeId != null ? PackageTypeId.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Weight.GetHashCode();
                hashCode = (hashCode * 397) ^ Length.GetHashCode();
                hashCode = (hashCode * 397) ^ Height.GetHashCode();
                hashCode = (hashCode * 397) ^ Width.GetHashCode();
                hashCode = (hashCode * 397) ^ Quantity;
                hashCode = (hashCode * 397) ^ (Contents != null ? Contents.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsFragile.GetHashCode();
                hashCode = (hashCode * 397) ^ Volume.GetHashCode();
                return hashCode;
            }
        }
    }
}
