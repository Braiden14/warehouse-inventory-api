﻿using System;

namespace WarehouseSystem.Models
{
    public class PackageType : IEquatable<PackageType>
    {
        public Guid Id { get; set; }
        public string Type { get; set; }

        public bool Equals(PackageType other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && string.Equals(Type, other.Type);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((PackageType) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Id.GetHashCode() * 397) ^ (Type != null ? Type.GetHashCode() : 0);
            }
        }
    }
}
